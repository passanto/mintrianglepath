import scala.annotation.tailrec
import scala.collection.immutable

object MinTrianglePath {

  // case class representing the tree
  case class Tree[Int](value: Int, left: Option[Tree[Int]], right: Option[Tree[Int]])

  def getRowsNumFromLine(args: Array[String]): Int = {
    try
      args.foreach(_.toInt)
    catch {
      case nfe: NumberFormatException => println("Input have to be integers:\n" + nfe)
    }
    val argsLen = args.length
    // checks if (1 + 8 * argsLen) is a perfect square
    val x = 1 + 8 * argsLen
    val sqX: Int = math.sqrt(x).intValue()
    if (math.pow(sqX, 2) != x)
      throw new IllegalArgumentException("Input is not valid.")

    val rowNum = (math.sqrt(1 + 8 * argsLen) - 1) / 2
    rowNum.intValue()
  }

  def createArrayFromInput(args: Array[String], rows: Int): immutable.IndexedSeq[immutable.IndexedSeq[Int]] = {
    for (i <- 0 until rows) yield {
      val startIndex = i * (i + 1) / 2
      val numOfElems = (i * (i + 1) / 2) + i + 1
      for (j <- startIndex until numOfElems) yield {
        args(j).toInt
      }
    }
  }

  def buildTree(lines: IndexedSeq[IndexedSeq[Int]]): Option[Tree[Int]] = {
    def buildTreeRecurse(lines: IndexedSeq[IndexedSeq[Int]]): IndexedSeq[Tree[Int]] = lines match {
      case line +: IndexedSeq() =>
        line.map(Tree(_, None, None))
      case line +: rest =>
        val nextRow = buildTreeRecurse(rest)
        (line, nextRow.sliding(2).toIndexedSeq).zipped.map {
          case (v, IndexedSeq(left, right)) =>
            val weight = v + math.min(left.value, right.value)
            Tree(weight, Some(left), Some(right))
        }
    }

    buildTreeRecurse(lines).headOption
  }

  def minPathWeight(matrix: Option[Tree[Int]]): Array[Int] = {
    @tailrec
    def minWeightRecurse(matrix: Option[Tree[Int]], acc: Array[Int]): Array[Int] = matrix match {
      case Some(Tree(x: Int, left: Option[Tree[Int]], right: Option[Tree[Int]])) =>
        (left, right) match {
          case (Some(_), Some(_)) =>
            val min = if (left.get.value < right.get.value) left else right
            val v = x - min.get.value
            minWeightRecurse(min, acc :+ v)
          case (_, _) => acc :+ x
        }
      case _ => acc
    }

    minWeightRecurse(matrix, Array.emptyIntArray)
  }

  def main(args: Array[String]): Unit = {
    // validates the input
    // and calculates the number of levels/rows
    val rows = getRowsNumFromLine(args)

    // creates a bidimensional array from the input string
    val arr = createArrayFromInput(args, rows)

    // creates the Tree structure from the bidimensional array
    val tree = buildTree(arr)

    // finds the minimal path nodes
    val res = minPathWeight(tree)

    println("Minimal path is: " + res.deep.mkString(" + ") + " = " + res.sum)
  }

}
