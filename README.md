# MinTrianglePath solution

This solution reads a text-triangle from STD input and outputs the minimal path 

## Cloning the repository

```
$ git clone git@bitbucket.org:passanto/mintrianglepath.git 
```


### Prerequisites

* Java
* SBT
  
### Assumptions

* The input is a string representing a full tree:   
  * valid input:  
    ```7 6 3 3 8 5 11 2 10 9```
  * not valid input:  
    ```7 6 3 3 8 5 11 2 10```
* The input string is made of integers separated by one or more spaces
  
## Running the program

Navigate to the project root folder: 

```
$ cd mintrianglepath
```

Run the program as required:   
(mind the double quote)
```
$ cat << EOF | sbt "run
7
6 3
3 8 5
11 2 10 9"
EOF
```
the same result is obtained without new line:   
```
$ cat << EOF | sbt "run 7 6 3 3 8 5 11 2 10 9"
EOF
```
Output should be as expected: 

```
Minimal path is : 7 + 6 + 3 + 2 = 18
```
## Debugging the program

Navigate to the project root folder: 

```
$ cd mintrianglepath
```
Run the program as required:   
(mind the double quote)
```
$ cat << EOF | sbt -J-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005 "run
7
6 3
3 8 5
11 2 10 9"
EOF
```
Configure and launch a remote configuration on port 5005